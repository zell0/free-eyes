FROM node:8

COPY . /var/www/build/

WORKDIR /var/www/build/

RUN \
   yarn install &&\
   yarn build:development

CMD cp .htaccess /var/www/front/.htaccess && cp -rpv /var/www/build/build/* /var/www/front/
