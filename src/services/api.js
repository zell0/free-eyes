class Api {
  // _apiBase = 'https://free-eyes.com/api';

  // _proxy = 'https://cors-anywhere.herokuapp.com/';

  getResource = async (url) => {
    const res = await fetch(url);

    if (!res.ok) {
      throw new Error(`Could not fetch ${url} : ${res.status}`);
    }

    return res.json();
  };

  getAllArticles = async () => (
    this.getResource(`/data.json`)
      .then((data) => data.articles)
  );

  getArticlesById = async (id) => (
    this.getAllArticles()
      .then((articles) => articles.filter((article) => article.category_id === id))
  );

  getArticle = async (url) => (
    this.getAllArticles()
      .then((articles) => articles.find((article) => article.url === url))
  );

  getAllCategories = async () => (
    this.getResource(`/data.json`)
      .then((data) => ({
        categoriesList: data.categories,
        subcategoriesList: data.subcategories
      }))
  );

  getCategory = async (url) => (
    this.getAllCategories()
      .then((categories) => categories.subcategoriesList.find((cat) => cat.url === url))
  );
}

export default new Api();
