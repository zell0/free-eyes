import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ErrorIndicator from 'components/ErrorIndicator';

class ErrorBoundary extends Component {
  constructor() {
    super();

    this.state = {
      error: false
    };
  }

  componentDidCatch() {
    this.setState({ error: true });
  }

  render() {
    if (this.state.error) {
      return <ErrorIndicator />;
    }

    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.objectOf(PropTypes.any)
};

export default ErrorBoundary;
