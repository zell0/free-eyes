import React from 'react';

import Logo from 'components/elements/Logo';
import Social from 'components/SocialList';
import { Wrapper } from 'styles/common';
import {
  StyledHeader, Inner, Left, Right, LeftInner, RightInner
} from './styles';

const Header = () => (
  <StyledHeader>
    <Wrapper>
      <Inner>
        <Left>
          <LeftInner>
            <Logo />
          </LeftInner>
        </Left>
        <Right>
          <RightInner>
            <Social />
          </RightInner>
        </Right>
      </Inner>
    </Wrapper>
  </StyledHeader>
);

export default Header;
