import styled from 'styled-components/macro';

export const StyledHeader = styled.header``;

export const Inner = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  padding-top: 5px;
  margin: 0 -${({ theme }) => theme.INDENT}px;
`;

export const Left = styled.div`
  flex-grow: 0.5;
  margin-left: auto;
`;

export const Right = styled.div`
  flex-grow: 0.5;
  text-align: right;
  margin: 10px 0;
`;

export const LeftInner = styled.div`
  display: inline-block;
  text-align: center;
  margin: 0 ${({ theme }) => theme.INDENT}px;
`;

export const RightInner = styled.div`
  margin: 0 ${({ theme }) => theme.INDENT}px;
`;
