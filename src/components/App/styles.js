import styled from 'styled-components/macro';

import { device } from 'constants/device';

export const StyledApp = styled.div`
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
`;

export const AppTop = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1 0 auto;
`;

export const AppBottom = styled.div``;

export const AppContent = styled.div`
  display: flex;
  padding: ${({ theme }) => theme.GUTTER}px 0;
  
  ${device.laptop} {
    flex-direction: column;
    align-items: center;
    padding-bottom: 10px;
  }
  
  ${device.tablet} {
    width: 100%;
    max-width: 500px;
    margin-left: auto;
    margin-right: auto;
  }
`;

export const AppMain = styled.main`
  flex-grow: 1;
  width: 100%;
  margin-right: ${({ theme }) => theme.INDENT}px;
  
  ${device.laptop} {
    margin-right: 0;
    margin-bottom: ${({ theme }) => theme.GUTTER}px;
  }
`;

export const AppSidebar = styled.aside`
  flex-shrink: 0;
  width: 324px;
  margin-left: ${({ theme }) => theme.INDENT}px;
  
  ${device.laptop} {
    margin-left: 0;
  }
  
  ${device.mobileL} {
    width: 320px;
  }
`;
