import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

// Styles
import { GlobalStyle } from 'styles/global';
import {
  StyledApp, AppTop, AppBottom, AppMain, AppContent, AppSidebar
} from './styles';
import { Wrapper } from 'styles/common';

// Pages
import HomePageContainer from 'pages/HomePageContainer';
import AboutPage from 'pages/AboutPage';
import ContactsPage from 'pages/ContactsPage';
import ArticlePageContainer from 'pages/ArticlePageContainer';
import CategoryPageContainer from 'pages/CategoryPageContainer';
import ErrorPage from 'pages/ErrorPage';

// Components
import Header from 'components/Header';
import Footer from 'components/Footer';
import NavigationContainer from 'components/NavigationContainer';
import SidebarContainer from 'components/SidebarContainer';
import ErrorBoundary from 'components/ErrorBoundary';

const App = () => (
  <StyledApp>
    <GlobalStyle />
    <AppTop>
      <Header />
      <NavigationContainer />
      <Wrapper>
        <AppContent>
          <AppMain>
            <ErrorBoundary>
              <Switch>
                <Route path="/" exact component={HomePageContainer} />
                <Route path="/about/" exact component={AboutPage} />
                <Route path="/contacts/" exact component={ContactsPage} />
                <Route path="/:url/" exact component={ArticlePageContainer} />
                <Route path="/category/:url/" exact component={CategoryPageContainer} />
                <Route component={ErrorPage} />
              </Switch>
            </ErrorBoundary>
          </AppMain>
          <AppSidebar>
            <ErrorBoundary>
              <SidebarContainer />
            </ErrorBoundary>
          </AppSidebar>
        </AppContent>
      </Wrapper>
    </AppTop>
    <AppBottom>
      <Footer />
    </AppBottom>
  </StyledApp>
);

export default App;
