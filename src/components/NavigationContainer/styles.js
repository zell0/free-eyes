import styled, { css } from 'styled-components/macro';

export const StyledNavigation = styled.nav`
  height: 42px;
  border-top: 1px solid ${({ theme }) => theme.GREYLIGHT};
  border-bottom: 1px solid ${({ theme }) => theme.GREYLIGHT};
  background-color: ${({ theme }) => theme.GREYLIGHTEST};

  ${({ isFixed }) => isFixed && css`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 12;
  `}
`;

export const Inner = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Left = styled.div`
  flex-grow: 1;
`;

export const Right = styled.div`
  padding: 4px 0;
`;
