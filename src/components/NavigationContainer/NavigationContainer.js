import React, {
  useRef, useState, useEffect, useLayoutEffect, useCallback
} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import MenuList from 'components/MenuList';
import SearchForm from 'components/forms/SearchForm';
import Burger from 'components/elements/Burger';
import { useOnClickOutside } from 'hooks';
import { setWebsiteLayout } from 'store/actions';

import { debounce } from 'utils';

import { Wrapper } from 'styles/common';
import {
  StyledNavigation, Inner, Left, Right
} from './styles';

const NavigationContainer = ({ setWebsiteLayout }) => {
  const navigationRef = useRef();
  const menuWrapperRef = useRef();
  const menuRef = useRef();

  const [menuWidth, setMenuWidth] = useState(0);
  const [menuWrapperWidth, setMenuWrapperWidth] = useState(0);
  const [desktopMenu, setDesktopMenu] = useState(true);
  const [openMenu, setOpenMenu] = useState(false);

  const toggleMenu = useCallback(() => {
    setDesktopMenu(menuWrapperWidth > menuWidth);
    setWebsiteLayout({
      mobile: !desktopMenu,
      navigationHeight: navigationRef.current.clientHeight
    });
  }, [menuWidth, menuWrapperWidth, desktopMenu, setWebsiteLayout]);

  const handleToggleMenu = useCallback(() => {
    setMenuWrapperWidth(menuWrapperRef.current.clientWidth);
    toggleMenu();
  }, [toggleMenu]);

  useLayoutEffect(() => handleToggleMenu(), [handleToggleMenu]);

  useEffect(() => setMenuWidth(menuRef.current.clientWidth), []);

  useEffect(() => {
    const debounceHandleToggleMenu = debounce(handleToggleMenu, 100);
    debounceHandleToggleMenu();
    window.addEventListener('resize', debounceHandleToggleMenu);

    return () => window.removeEventListener('resize', debounceHandleToggleMenu);
  }, [menuWrapperWidth, handleToggleMenu]);

  useEffect(() => {
    document.body.style.paddingTop = desktopMenu ? 0 : `${navigationRef.current.clientHeight}px`;
  }, [desktopMenu]);

  useOnClickOutside(navigationRef, () => setOpenMenu(false));

  return (
    <StyledNavigation ref={navigationRef} isFixed={!desktopMenu}>
      <Wrapper>
        <Inner>
          <Left ref={menuWrapperRef}>
            {(desktopMenu || openMenu) && <MenuList ref={menuRef} show={desktopMenu} open={openMenu} />}
            {!desktopMenu && <Burger open={openMenu} setOpen={setOpenMenu} />}
          </Left>
          <Right>
            <SearchForm />
          </Right>
        </Inner>
      </Wrapper>
    </StyledNavigation>
  );
};

NavigationContainer.propTypes = {
  setWebsiteLayout: PropTypes.func.isRequired
};

export default connect(
  ({ layout: { mobile } }) => ({
    mobile
  }),
  { setWebsiteLayout }
)(NavigationContainer);
