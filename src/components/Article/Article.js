import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import Rating from 'react-rating';

import Typography from 'components/elements/Typography';
import StarIcon from 'components/elements/StarIcon';
import { lazyloadImages } from 'utils';

import {
  StyledArticle, Date, ImageTop, StyledRating, RatingText
} from './styles';
import { Content } from 'styles/common';

const Article = ({ article, article: { rating }, setRateArticle }) => {
  const averageRating = rating ? (rating.total / rating.amount).toFixed(2) : 0;
  const isRated = localStorage.getItem(`rate-${article.id}`);
  const content = useRef();

  const parseDate = (isoFormatDate) => {
    if (isoFormatDate) {
      const date = isoFormatDate.split('T')[0].split('-');
      return [...date].reverse().join('.');
    }

    return null;
  };

  const date = parseDate(article.date);

  const handleClickRate = (value) => setRateArticle(article.id, value);

  useEffect(() => lazyloadImages(content.current), []);

  return (
    <StyledArticle ref={content}>
      <Typography style={{ marginBottom: 0 }}>{article.title}</Typography>
      <Date>{date}</Date>
      <ImageTop
        src="data:image/gif;base64,R0lGODlhCAAFAIAAAP///wAAACH5BAEAAAEALAAAAAAIAAUAAAIFjI+py1gAOw=="
        data-src={article.thumbnail}
        className="lazy"
        width={article.thumbnail_width}
        height={article.thumbnail_height}
        alt={article.title}
      />
      <Content dangerouslySetInnerHTML={{ __html: article.content }} />
      <StyledRating>
        <Rating
          style={{ flexShrink: 0 }}
          initialRating={averageRating}
          emptySymbol={<StarIcon href="#empty" />}
          fullSymbol={<StarIcon href="#full" />}
          onClick={handleClickRate}
          readonly={isRated}
        />
        <RatingText>{`(${averageRating}) ${isRated ? 'You have already rated this article' : ''}`}</RatingText>
      </StyledRating>
    </StyledArticle>
  );
};

Article.propTypes = {
  article: PropTypes.objectOf(PropTypes.any).isRequired,
  setRateArticle: PropTypes.func.isRequired
};

export default Article;
