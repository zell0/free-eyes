import styled from 'styled-components/macro';

export const StyledArticle = styled.article``;

export const Date = styled.div`
  font-size: 12px;
  color: ${({ theme }) => theme.GREY};
  margin-top: 10px;
  margin-bottom: 20px;
`;

export const ImageTop = styled.img`
  display: block;
  margin-bottom: 20px;
  background-color: ${({ theme }) => theme.GREYLIGHT};
`;

export const StyledRating = styled.div`
  display: flex;
  margin-top: 20px;
`;

export const RatingText = styled.span`
  font-size: 14px;
  font-weight: 700;
  margin-left: 8px;
`;
