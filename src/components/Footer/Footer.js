import React from 'react';

import { StyledFooter } from './styles';

const Footer = () => (
  <StyledFooter>
    © Open your eyes
    <br />
    2014-
    {new Date().getFullYear()}
  </StyledFooter>
);

export default Footer;
