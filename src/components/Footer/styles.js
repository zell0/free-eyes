import styled from 'styled-components/macro';

export const StyledFooter = styled.footer`
  font-size: 13px;
  padding: 20px 15px;
  border-top: 1px solid ${({ theme }) => theme.GREYLIGHT};
  text-align: center;
`;
