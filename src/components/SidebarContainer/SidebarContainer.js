import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';

import Typography from 'components/elements/Typography';
import CategoryList from 'components/CategoryList';
import Loader from 'components/elements/Loader';
import ErrorIndicator from 'components/ErrorIndicator';
import { categoriesRequest } from 'store/actions';

import { StyledSidebar } from './styles';

class SidebarContainer extends Component {
  componentDidMount() {
    this.props.categoriesRequest();
  }

  render() {
    const { categories: { categoriesList, subcategoriesList }, loading, error } = this.props;

    if (loading) {
      return <Loader />;
    }

    if (error) {
      return <ErrorIndicator />;
    }

    return (
      <LazyLoad>
        <StyledSidebar>
          {
            categoriesList.map((category) => (
              <Fragment key={category.id}>
                <Typography as="h2" align="center">{category.title}</Typography>
                <CategoryList categories={subcategoriesList.filter((subcategory) => subcategory.category === category.id)} />
              </Fragment>
            ))
          }
        </StyledSidebar>
      </LazyLoad>
    );
  }
}

SidebarContainer.propTypes = {
  categories: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.object, PropTypes.array])).isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  categoriesRequest: PropTypes.func.isRequired
};

export default connect(
  ({ categories: { categories, loading, error } }) => ({
    categories,
    loading,
    error
  }),
  {
    categoriesRequest
  }
)(SidebarContainer);
