import styled from 'styled-components/macro';

import { device } from 'constants/device';

export const StyledArticleList = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -${({ theme }) => theme.INDENT}px;
`;

export const Item = styled.div`
  width: calc(50% - ${({ theme }) => theme.GUTTER}px);
  margin: 0 ${({ theme }) => theme.INDENT}px ${({ theme }) => theme.GUTTER}px;
  
  ${device.tablet} {
    width: 100%;
  }
`;
