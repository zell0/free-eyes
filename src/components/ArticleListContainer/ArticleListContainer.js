import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { parse } from 'query-string';
import LazyLoad from 'react-lazyload';

import ArticleListItem from 'components/ArticleListItem';
import Typography from 'components/elements/Typography';
import Pagination from 'components/Pagination';
import { setCurrentPage } from 'store/actions';

import { StyledArticleList, Item } from './styles';

const ArticleListContainer = ({
  articles, articlesPerPage, currentPage, setCurrentPage, location, noDataMessage
}) => {
  const pageCount = Math.ceil(articles.length / articlesPerPage);
  let curPage = currentPage;
  curPage = location.search.includes('page') ? Number(parse(location.search).page) : 1;

  const getArticleList = () => {
    const indexOfLastArticle = curPage * articlesPerPage;
    const indexOfFirstArticle = indexOfLastArticle - articlesPerPage;
    return articles.length ? articles.slice(indexOfFirstArticle, indexOfLastArticle) : [];
  };

  const getPagination = (pageCount) => pageCount > 1 && (
    <Pagination
      currentPage={curPage}
      pageCount={pageCount}
      setCurrentPage={setCurrentPage}
    />
  );

  const articleList = getArticleList().map((article) => (
    <Item key={article.id}>
      <LazyLoad>
        <ArticleListItem
          key={article.id}
          title={article.title}
          url={article.url}
          thumbnail={article.thumbnail}
          excerpt={article.content}
        />
      </LazyLoad>
    </Item>
  ));

  return (
    <>
      {
        articleList.length
          ? <StyledArticleList>{articleList}</StyledArticleList>
          : <Typography as="h3">{noDataMessage}</Typography>
      }
      {getPagination(pageCount)}
    </>
  );
};

ArticleListContainer.propTypes = {
  articles: PropTypes.arrayOf(PropTypes.any).isRequired,
  articlesPerPage: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  location: PropTypes.objectOf(PropTypes.any),
  noDataMessage: PropTypes.string.isRequired,
};

export default connect(
  ({ articles: { articlesPerPage, currentPage } }) => ({
    articlesPerPage,
    currentPage
  }),
  {
    setCurrentPage
  }
)(ArticleListContainer);
