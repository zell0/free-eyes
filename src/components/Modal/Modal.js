import React, {
  useState, useEffect, useRef, useLayoutEffect
} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import FocusTrap from 'focus-trap-react';

import Button from 'components/elements/Button';
import { useOnClickOutside } from 'hooks';

import {
  StyledModal, ModalBack, ModalWrapper, ModalBody, ModalBottom, CloseButton
} from './styles';

const Modal = ({
  isOpen, onClose, maxWidth, align, children, okButton
}) => {
  const scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
  const modalBody = useRef();
  const [show, setShow] = useState(false);

  useEffect(() => setShow(isOpen), [isOpen]);

  useLayoutEffect(() => {
    document.body.classList.add('overflow-hidden');
    document.body.style.paddingRight = `${scrollbarWidth}px`;

    return () => {
      document.body.classList.remove('overflow-hidden');
      document.body.style.paddingRight = 0;
    };
  }, []); // eslint-disable-line

  useOnClickOutside(modalBody, () => setShow(false));

  const onTransitionEnd = () => {
    if (show) {
      return;
    }

    close();
  };

  const onClosing = (e) => {
    if (e) {
      e.preventDefault();
    }
    setShow(false);
  };

  const close = () => onClose();

  const onEscKeyDown = (e) => {
    if (e.keyCode === 27) {
      onClosing();
    }
  };

  return ReactDOM.createPortal(
    <FocusTrap>
      <StyledModal
        role="dialog"
        tabIndex="-1"
        aria-modal="true"
        onKeyDown={onEscKeyDown}
        onTransitionEnd={onTransitionEnd}
      >
        <ModalBack />
        <ModalWrapper>
          <ModalBody
            isShow={show}
            maxWidth={maxWidth}
            align={align}
            ref={modalBody}
          >
            {children}
            {okButton ? (
              <ModalBottom>
                <Button
                  textColor={okButton.textColor}
                  bgColor={okButton.bgColor}
                  bgHover={okButton.bgHover}
                  bgActive={okButton.bgActive}
                  onClick={onClosing}
                >
                  OK
                </Button>
              </ModalBottom>
            ) : (
              // for focus
              <a href="/" onClick={(e) => e.preventDefault()}>{}</a>
            )}
            <CloseButton onClick={onClosing} />
          </ModalBody>
        </ModalWrapper>
      </StyledModal>
    </FocusTrap>,
    document.body
  );
};

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  maxWidth: PropTypes.number,
  align: PropTypes.string,
  children: PropTypes.objectOf(PropTypes.any)
};

export default Modal;
