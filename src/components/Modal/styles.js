import styled from 'styled-components/macro';

import { hexToRgba } from 'utils';
import icoClose from 'assets/images/icons/close.svg';

export const StyledModal = styled.aside`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1000;
`;

export const ModalBack = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => hexToRgba(theme.BLACK, 0.7)};
  z-index: 997;
`;

export const ModalWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-start;
  padding: 15px;
  overflow-y: auto;
  z-index: 998;
`;

export const ModalBody = styled.div`
  position: relative;
  width: 100%;
  max-width: ${({ maxWidth }) => maxWidth || 400}px;
  background-color: ${({ theme }) => theme.WHITE};
  margin: auto;
  padding: 40px 30px 20px;
  text-align: ${({ align }) => align || 'left'};
  border-radius: 5px;
  box-shadow: 0 12px 15px 0 ${({ theme }) => hexToRgba(theme.BLACK, 0.5)};
  transform: translateY(${({ isShow }) => (isShow ? 0 : `-${window.innerHeight}px`)});
  transition: transform linear .2s;
  z-index: 999;
`;

export const ModalBottom = styled.div`
  margin-top: 30px;
`;

export const CloseButton = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  width: 40px;
  height: 40px;
  background: url(${icoClose}) no-repeat center;
  background-size: 28px;
  cursor: pointer;
`;
