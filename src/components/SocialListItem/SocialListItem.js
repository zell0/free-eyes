import React from 'react';
import { ReactSVG } from 'react-svg';
import PropTypes from 'prop-types';

import { StyledLink } from './styles';

const SocialListItem = ({ title, href, srcImage }) => (
  <StyledLink
    href={href}
    target="_blank"
    rel="noopener nofollow"
    title={title}
  >
    <ReactSVG
      src={srcImage}
      beforeInjection={(svg) => svg.setAttribute('style', 'width: 22px; height: 22px')}
    />
  </StyledLink>
);

SocialListItem.propTypes = {
  title: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  srcImage: PropTypes.string.isRequired
};

export default SocialListItem;
