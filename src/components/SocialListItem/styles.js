import styled from 'styled-components/macro';

export const StyledLink = styled.a`
  width: 22px;
  height: 22px;
`;
