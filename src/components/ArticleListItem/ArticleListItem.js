import React, { forwardRef, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import objectFitImages from 'object-fit-images';
import lineClamp from 'line-clamp';
import LazyLoad, { forceCheck } from 'react-lazyload';

import {
  StyledArticleListItem, Title, Thumbnail, Image, Excerpt, Button
} from './styles';

const ArticleListItem = forwardRef(({
  title, url, thumbnail, excerpt
}, imageRef) => {
  const excerptRef = useRef();
  const excerptClipped = excerpt.replace(/<[^>]+>/g, '');

  useEffect(() => {
    objectFitImages(imageRef);
    lineClamp(excerptRef.current, 3);
    forceCheck();
  });

  return (
    <StyledArticleListItem>
      <Title to={`/${url}/`}>{title}</Title>
      <Thumbnail to={`/${url}/`}>
        <LazyLoad>
          <Image
            src={thumbnail}
            ref={imageRef}
            objectfit="cover"
            title={title}
            alt={title}
          />
        </LazyLoad>
      </Thumbnail>
      <Excerpt ref={excerptRef}>{excerptClipped}</Excerpt>
      <Button to={`/${url}/`}>Read more</Button>
    </StyledArticleListItem>
  );
});

ArticleListItem.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired,
  excerpt: PropTypes.string.isRequired
};

ArticleListItem.displayName = 'ArticleListItem';

export default ArticleListItem;
