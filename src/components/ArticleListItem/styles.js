import styled, { css } from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

import ImageFallback from 'components/elements/ImageFallback';

export const StyledArticleListItem = styled.article`
  display: block;
`;

export const Title = styled(NavLink)`
  display: block;
  font-size: 23px;
  font-weight: 700;
  line-height: 1.2;
  margin-bottom: 15px;
  
  :hover {
    color: ${({ theme }) => theme.PRIMARY};
  }
`;

export const Thumbnail = styled(NavLink)`
  position: relative;
  display: block;
  width: 100%;
  height: 0;
  padding-bottom: 69%;
  margin-bottom: 10px;
  background-color: ${({ theme }) => theme.GREYLIGHT};
  overflow: hidden;
`;

export const Image = styled(ImageFallback)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => theme.GREYLIGHT};
  transition: transform .2s ease;
  
  ${({ objectfit }) => objectfit && css`
    object-fit: ${objectfit};
    font-family: 'object-fit: ${objectfit};';
  `}
  
  :hover {
    transform: scale(1.1);
  }
`;

export const Excerpt = styled.div`
  margin-bottom: 12px;
`;

export const Button = styled(NavLink)`
  display: inline-block;
  padding: 3px 22px;
  font-size: 17px;
  font-weight: bold;
  font-style: italic;
  color: ${({ theme }) => theme.WHITE};
  background-color: ${({ theme }) => theme.SECONDARY};
  border: 2px solid transparent;
  
  :hover {
    color: ${({ theme }) => theme.SECONDARY};
    background-color: transparent;
    border-color: ${({ theme }) => theme.SECONDARY};
  }
`;
