import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'components/elements/Typography';

import { StyledCategory } from './styles';
import { Content } from 'styles/common';

const Category = ({ category }) => (
  <StyledCategory>
    <Typography>{category.title}</Typography>
    <Content dangerouslySetInnerHTML={{ __html: category.content }} />
  </StyledCategory>
);

Category.propTypes = {
  category: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Category;
