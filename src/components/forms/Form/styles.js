import styled from 'styled-components/macro';

import { device } from 'constants/device';

export const StyledForm = styled.form`
  max-width: 400px;
  margin-bottom: 30px;
  
  ${device.tablet} {
    max-width: 100%;
    text-align: center;
  }
`;

export const FormInner = styled.div`
  margin-bottom: 30px;
`;
