import React, { useState, useEffect } from 'react';
import { darken } from 'polished';

import FormControl from 'components/forms/FormControl';
import Button from 'components/elements/Button';
import { formControls as controls } from 'utils/data';
import validate from 'utils/validate';
import Typography from 'components/elements/Typography';
import Loader from 'components/elements/Loader';
import Modal from 'components/Modal';

import { StyledForm, FormInner } from './styles';

const Form = () => {
  const [isValid, setIsValid] = useState(false);
  const [isSent, setIsSent] = useState(false);
  const [formControls, setFormControls] = useState({ ...controls });
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [openModal, setOpenModal] = useState(false);

  const handleChange = (e) => {
    const { name } = e.target;
    const { value } = e.target;
    const updatedControls = { ...formControls };
    const updatedFormElement = { ...updatedControls[name] };

    updatedFormElement.value = value;
    updatedFormElement.touched = true;
    updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
    updatedControls[name] = updatedFormElement;

    let isValid = true;

    Object.keys(updatedControls).forEach((control) => {
      if (updatedControls.hasOwnProperty(control)) {
        isValid = updatedControls[control].valid && isValid;
      }
    });

    setFormControls(updatedControls);
    setIsValid(isValid);
  };

  const handleBlur = () => {
    const updatedControls = { ...formControls };

    Object.keys(updatedControls).forEach((control) => {
      if (updatedControls.hasOwnProperty(control)) {
        updatedControls[control].active = !!(updatedControls[control].value.trim());
      }
    });

    setFormControls(updatedControls);
  };

  const updateControls = () => {
    const updatedControls = { ...formControls };

    Object.keys(updatedControls).forEach((control) => {
      if (updatedControls.hasOwnProperty(control)) {
        if (!updatedControls[control].valid) {
          updatedControls[control].touched = true;
        }
      }
    });

    setFormControls(updatedControls);
  };

  const clearControls = () => {
    const updatedControls = { ...formControls };

    Object.keys(updatedControls).forEach((control) => {
      if (updatedControls.hasOwnProperty(control)) {
        updatedControls[control].active = false;
        updatedControls[control].value = '';
        updatedControls[control].touched = false;
        updatedControls[control].valid = false;
      }
    });

    setFormControls(updatedControls);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!isValid) {
      updateControls();
      setIsSent(false);
      return;
    }

    const formData = {};

    Object.keys(formControls).forEach((control) => {
      if (formControls.hasOwnProperty(control)) {
        formData[control] = formControls[control].value;
      }
    });

    // emulation of sending
    setIsSubmitting(true);
    const random = (min, max) => Math.floor(min + Math.random() * (max + 1 - min));

    setTimeout(() => {
      setIsSubmitting(false);
      setIsSent(true);
      clearControls();
      setIsValid(false);
      setOpenModal(true);
    }, random(100, 1000));
  };

  const modalContent = (
    <>
      <Typography as="h4" align="center">Your message is sent</Typography>
      <p>Thank you! We will be in touch very soon.</p>
    </>
  );

  const closeModal = () => setOpenModal(false);

  useEffect(() => {
    clearControls();

    return () => clearControls();
  }, []); // eslint-disable-line

  return (
    <>
      <StyledForm onSubmit={handleSubmit}>
        <FormInner>
          {
            Object.keys(formControls).map((control, idx) => (
              <FormControl
                key={idx}
                as={formControls[control].type}
                id={formControls[control].name}
                name={formControls[control].name}
                label={formControls[control].label}
                active={formControls[control].active}
                value={formControls[control].value}
                touched={formControls[control].touched}
                valid={formControls[control].valid}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            ))
          }
          <Button type="submit" disabled={isSubmitting}>Send</Button>
        </FormInner>
        {isSubmitting && <Loader size="small" />}
      </StyledForm>
      {!isSubmitting && isSent && openModal && (
        <Modal
          isOpen={openModal}
          onClose={closeModal}
          maxWidth={400}
          align="center"
          okButton={{
            textColor: ({ theme }) => theme.WHITE,
            bgColor: ({ theme }) => theme.PRIMARY,
            bgHover: ({ theme }) => darken(0.1, theme.PRIMARY),
            bgActive: ({ theme }) => darken(0.15, theme.PRIMARY)
          }}
        >
          {modalContent}
        </Modal>
      )}
    </>
  );
};

export default Form;
