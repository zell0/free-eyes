import React from 'react';
import PropTypes from 'prop-types';

import { StyledFormControl, Field, Label } from './styles';

const FormControl = (props) => {
  const {
    name, label, touched, valid
  } = props;
  let invalid;

  if (touched && !valid) {
    invalid = true;
  }

  return (
    <StyledFormControl>
      <Field
        {...props}
        invalid={invalid}
      />
      <Label htmlFor={name}>{label}</Label>
    </StyledFormControl>
  );
};

FormControl.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  touched: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
};

export default FormControl;
