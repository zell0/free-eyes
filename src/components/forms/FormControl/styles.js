import styled from 'styled-components/macro';

const labelActive = `
  top: -20px;
  font-size: 12px;
`;

export const StyledFormControl = styled.div`
  width: 100%;
  position: relative;
  margin: 45px 0 25px;
`;

export const Label = styled.label`
  position: absolute;
  top: 0;
  left: 0;
  color: ${({ theme }) => theme.GREY};
  cursor: text;
  transition: .2s;
`;

export const Field = styled.input`
  width: 100%;
  box-shadow: none;
  border: none;
  border-bottom: 1px solid ${({ invalid }) => (invalid ? ({ theme }) => theme.ERROR : ({ theme }) => theme.GREYLIGHT)};
  padding: 5px 3px;
  outline: 0;
  transition: border-color .2s ease;
  
  ${({ as }) => as === 'textarea' && `
    min-width: 100%;
    max-width: 100%;
    min-height: 70px;
  `}

  :focus {
    border-color: ${({ invalid }) => (invalid ? ({ theme }) => theme.ERROR : ({ theme }) => theme.ACTIVE)};

    + ${Label} {
      ${labelActive}
    }
  }

  ${({ active }) => active && `
    + ${Label} {
      ${labelActive}
    }
  `}
`;
