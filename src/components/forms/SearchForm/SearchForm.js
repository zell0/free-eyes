import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { Form, Input, Submit } from './styles';

class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();

    this.state = {
      value: '',
      error: false
    };
  }

  onChange = (e) => {
    const { value } = e.target;
    const error = this.onError(value);

    this.setState({
      value,
      error
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    const error = this.onError(this.state.value);

    if (error) {
      this.inputFocus();
      this.setState({
        value: '',
        error
      });

      return;
    }

    this.props.history.push(`/?search=${this.state.value}`);
  };

  onError = (value) => value.trim().length <= 0;

  inputFocus = () => this.input.current.focus();

  onBlur = () => this.setState({ error: false });

  render() {
    const { value, error } = this.state;

    return (
      <Form role="search" onSubmit={this.onSubmit}>
        <Input
          value={value}
          onChange={this.onChange}
          error={error}
          ref={this.input}
          onBlur={this.onBlur}
          placeholder="Search"
        />
        <Submit />
      </Form>
    );
  }
}

SearchForm.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired
};

export default withRouter(SearchForm);
