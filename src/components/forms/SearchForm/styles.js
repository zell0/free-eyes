import styled from 'styled-components/macro';
import { hexToRgba } from 'utils';
import searchIcon from 'assets/images/icons/search.svg';

export const Form = styled.form`
  display: flex;
  align-items: center;
  height: 32px;
`;

export const Input = styled.input.attrs(() => ({
  type: 'text'
}))`
  height: 100%;
  font-size: 15px;
  border-radius: 30px;
  padding: 0 15px;
  outline: 0;
  transition: .2s ease;
  border: 1px solid ${(props) => (props.error ? props.theme.ERROR : props.theme.GREYLIGHT)};
  
  :focus {
    box-shadow: 0 0 8px ${(props) => (props.error && props.theme.ERROR ? hexToRgba(props.theme.ERROR, 0.6) : hexToRgba(props.theme.ACTIVE, 0.6))};
    border-color: ${(props) => (props.error ? props.theme.ERROR : props.theme.ACTIVE)};
  }
`;

export const Submit = styled.input.attrs(() => ({
  type: 'submit',
  value: ''
}))`
  width: 32px;
  height: 100%;
  border: none;
  cursor: pointer;
  padding: 0;
  margin-left: 5px;
  background: url(${searchIcon}) no-repeat center;
  background-size: 80%;
`;
