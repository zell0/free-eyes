import styled from 'styled-components/macro';

export const StyledPagination = styled.nav`
  margin: ${({ theme }) => theme.GUTTER}px 0;
  
  .pagination {
    display: flex;
    justify-content: center;
    
    &__item {
      margin-right: 8px;
      
      :last-child {
        margin-right: 0;
      }
      
      &.active {
      
        a {
          color: ${({ theme }) => theme.WHITE};
          background: ${({ theme }) => theme.PRIMARY};
          border-color: ${({ theme }) => theme.PRIMARY};
        }
      }
    }
    
    &__link {
      display: flex;
      justify-content: center;
      align-items: center;
      min-width: 35px;
      height: 35px;
      color: ${({ theme }) => theme.BLACK};
      background: none;
      border: 1px solid ${({ theme }) => theme.GREYLIGHT};
      text-align: center;
      padding: 0 10px;
      cursor: pointer;
      transition: .2s ease;
      
      :hover,
      :focus {
        outline: 0;
        border-color: ${({ theme }) => theme.PRIMARY};
      }
      
      img {
        max-width: 13px;
      }
    }
  }
`;

export const Image = styled.img``;
