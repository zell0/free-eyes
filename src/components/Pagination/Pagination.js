import React from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import { withRouter } from 'react-router-dom';
import { parse } from 'query-string';

import prevIcon from 'assets/images/icons/prev.svg';
import nextIcon from 'assets/images/icons/next.svg';

import { StyledPagination, Image } from './styles';

const Pagination = ({
  currentPage, pageCount, setCurrentPage, location, history
}) => {
  const pathname = location.pathname || '';
  let url = '';
  let urlWithPage = '';
  const searchParams = parse(location.search).search;

  if (searchParams) {
    url = `${pathname}?search=${searchParams}`;
    urlWithPage = `${url}&page`;
  } else {
    url = pathname;
    urlWithPage = `${url}?page`;
  }

  const handleChange = (data) => {
    const pageNum = data.selected + 1;
    const urlPush = (pageNum === 1) ? url : `${urlWithPage}=${pageNum}`;
    history.push(urlPush);
    setCurrentPage(pageNum);
  };

  return (
    <StyledPagination>
      <ReactPaginate
        previousLabel={<Image src={prevIcon} />}
        nextLabel={<Image src={nextIcon} />}
        pageCount={pageCount}
        forcePage={currentPage - 1}
        onPageChange={handleChange}
        marginPagesDisplayed={1}
        pageRangeDisplayed={2}
        breakLabel="..."
        containerClassName="pagination"
        breakClassName="pagination__item"
        breakLinkClassName="pagination__link"
        pageClassName="pagination__item"
        pageLinkClassName="pagination__link"
        previousClassName="pagination__item"
        nextClassName="pagination__item"
        previousLinkClassName="pagination__link"
        nextLinkClassName="pagination__link"
        activeClassName="active"
        hrefBuilder={(pageNum) => (pageNum !== 1 ? `${urlWithPage}=${pageNum}` : url)}
      />
    </StyledPagination>
  );
};

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  pageCount: PropTypes.number.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  location: PropTypes.objectOf(PropTypes.any),
  history: PropTypes.objectOf(PropTypes.any)
};

export default withRouter(Pagination);
