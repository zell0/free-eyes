import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

import ImageFallback from 'components/elements/ImageFallback';

import { hexToRgba } from 'utils';
import { device } from 'constants/device';

export const Link = styled(NavLink)`
  position: relative;
  display: block;
  width: 160px;
  height: 80px;
  margin: 0 0 4px;
  
  ${device.mobileL} {
    margin: 0;
  }
`;

export const Image = styled(ImageFallback)`
  display: block;
  background-color: ${({ theme }) => theme.GREYLIGHT};
`;

export const Title = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: ${({ theme }) => theme.WHITE};
  background-color: ${({ theme }) => theme.BLACK && hexToRgba(theme.BLACK, 0.6)};
  font-size: 17px;
  font-weight: 700;
  line-height: 1.2;
  text-transform: uppercase;
  padding: 10px 15px;
  transition: opacity .2s ease;
  
  :hover {
    opacity: 0;
  }
`;
