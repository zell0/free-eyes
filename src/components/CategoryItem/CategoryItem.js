import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';

import { Link, Image, Title } from './styles';

const CategoryItem = ({ href, srcImage, title }) => (
  <Link to={`/category/${href}/`} title={title}>
    <LazyLoad>
      <Image src={srcImage} alt={title} />
    </LazyLoad>
    <Title>{title}</Title>
  </Link>
);

CategoryItem.propTypes = {
  href: PropTypes.string.isRequired,
  srcImage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default CategoryItem;
