import React from 'react';
import Typography from 'components/elements/Typography';

const ErrorIndicator = () => (
  <Typography as="h2" style={{ textAlign: 'center' }}>Error! Please, try again later</Typography>
);

export default ErrorIndicator;
