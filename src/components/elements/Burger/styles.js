import styled from 'styled-components/macro';

export const StyledBurger = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 32px;
  height: 22px;
  cursor: pointer;
  z-index: 10;

  div {
    display: block;
    width: 100%;
    height: 4px;
    background-color: ${({ theme }) => theme.BLACK};
    border-radius: 5px;
    transform-origin: 1px;
    transition: 0.2s linear;

    :first-child {
      width: ${({ open }) => (open ? '85.5%' : '100%')};
      transform: ${({ open }) => (open ? 'rotate(45deg)' : 'rotate(0)')};
    }

    :nth-child(2) {
      opacity: ${({ open }) => (open ? 0 : 1)};
      transform: ${({ open }) => (open ? 'translateX(100%)' : 'translateX(0)')};
    }

    :nth-child(3) {
      width: ${({ open }) => (open ? '85.5%' : '100%')};
      transform: ${({ open }) => (open ? 'rotate(-45deg)' : 'rotate(0)')};
    }
  }
`;
