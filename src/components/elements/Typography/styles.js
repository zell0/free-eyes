import styled, { css } from 'styled-components/macro';

export const StyledTypography = styled.h1`
  font-size: 32px;
  font-weight: bold;
  line-height: 1;
  margin: 0 0 20px;
  color: ${(props) => props.color || props.theme.BLACK};
  text-align: ${(props) => props.align || 'left'};
  
  ${(props) => props.as === 'h2' && css`
    font-size: 22px;
  `}
  
  ${(props) => (props.as === 'h3' || props.as === 'h4') && css`
    font-size: 20px;
  `}
`;
