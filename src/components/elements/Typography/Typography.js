import React from 'react';
import PropTypes from 'prop-types';

import { StyledTypography } from './styles';

const Typography = (props) => (
  <StyledTypography {...props}>
    {props.children}
  </StyledTypography>
);

Typography.propTypes = {
  children: PropTypes.string
};

export default Typography;
