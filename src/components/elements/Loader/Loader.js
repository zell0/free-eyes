import React from 'react';
import PropTypes from 'prop-types';

import { StyledLoader, Wrap, Inner } from './styles';

const Loader = ({ size }) => (
  <StyledLoader>
    <Wrap size={size}>
      <Inner size={size} />
    </Wrap>
  </StyledLoader>
);

Loader.propTypes = {
  size: PropTypes.string
};

export default Loader;
