import styled, { keyframes } from 'styled-components/macro';

export const StyledLoader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Wrap = styled.div`
  position: relative;
  width: ${({ size }) => (size === 'small' ? '50px' : '100px')};
  height: ${({ size }) => (size === 'small' ? '50px' : '100px')};
  transform: translate(-50px, -50px) scale(0.5) translate(50px, 50px);
`;

const rotate = keyframes`
  0 % {
    transform: rotate(0deg);
  }
  50% {
    transform: rotate(180deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const Inner = styled.div`
  position: absolute;
  animation: ${rotate} .8s linear infinite;
  width: ${({ size }) => (size === 'small' ? '80px' : '160px')};
  height: ${({ size }) => (size === 'small' ? '80px' : '160px')};
  top: 20px;
  left: 20px;
  border-radius: 50%;
  box-shadow: 0 6px 0 0 ${({ theme }) => theme.ACTIVE};
  transform-origin:
    ${({ size }) => (size === 'small' ? '40px' : '80px')}
    ${({ size }) => (size === 'small' ? '41px' : '82px')};
`;
