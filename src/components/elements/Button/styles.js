import styled from 'styled-components/macro';
import { darken, lighten } from 'polished';

export const StyledButton = styled.button`
  position: relative;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  min-width: 100px;
  padding: 10px 20px;
  border: none;
  color: ${({ theme, textColor }) => textColor || theme.WHITE};
  background-color: ${({ theme, bgColor }) => bgColor || theme.PRIMARY};
  cursor: pointer;
  transition: background-color .2s ease;
  
  :focus {
    box-shadow: 0 0 0 1px red;
  }
  
  :hover {
    background-color: ${({ theme, bgHover }) => bgHover || darken(0.1, theme.PRIMARY)};
  }
  
  :active {
    background-color: ${({ theme, bgActive }) => bgActive || darken(0.15, theme.PRIMARY)};
  }
  
  :disabled {
    background-color: ${({ theme, bgDisabled }) => bgDisabled || lighten(0.3, theme.PRIMARY)};
  }
`;

export const Wrapper = styled.div`
  .ripple {
    animation-duration: .3s;
    opacity: .3;
  }
`;
