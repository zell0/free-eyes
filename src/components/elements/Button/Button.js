import React from 'react';
import PropTypes from 'prop-types';
import Ripple from 'react-material-ripple';

import { Wrapper, StyledButton } from './styles';

// main props: textColor, bgColor, bgHover, bgActive, bgDisabled
const Button = (props) => (
  <Wrapper>
    <Ripple>
      <StyledButton {...props}>
        {props.children}
      </StyledButton>
    </Ripple>
  </Wrapper>
);

Button.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.string.isRequired
};

export default Button;
