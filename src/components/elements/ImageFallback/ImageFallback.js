import React from 'react';
import PropTypes from 'prop-types';

const ImageFallback = ({
  src, fallback = src, type = 'image/webp', alt, ...delegated
}) => (
  <picture>
    <source srcSet={src} type={type} />
    <img src={fallback} alt={alt} {...delegated} />
  </picture>
);

ImageFallback.propTypes = {
  src: PropTypes.string.isRequired,
  fallback: PropTypes.string,
  type: PropTypes.string,
  alt: PropTypes.string
};

export default ImageFallback;
