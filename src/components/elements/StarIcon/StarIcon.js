import React from 'react';
import PropTypes from 'prop-types';

const StarIcon = ({ href }) => (
  <svg style={{ width: '20px', height: '20px' }} viewBox="0 0 20 19">
    <path
      id="empty"
      fill="#7e979d"
      d="M10 0l2.36 7.28L20 7.25l-6.19 4.47L16.19 19 10 14.48 3.83 19l2.36-7.28L0 7.25l7.66.03z"
    />
    <path
      id="full"
      fill="#EFD44E"
      d="M10 0l2.36 7.28L20 7.25l-6.19 4.47L16.19 19 10 14.48 3.83 19l2.36-7.28L0 7.25l7.66.03z"
    />
    <use xlinkHref={href} />
  </svg>
);

StarIcon.propTypes = {
  href: PropTypes.string.isRequired
};

export default StarIcon;
