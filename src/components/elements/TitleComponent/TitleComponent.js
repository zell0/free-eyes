import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { SITE_NAME } from 'constants/common';

const TitleComponent = ({ title }) => (
  <Helmet>
    <title>{(title && `${title} | ${SITE_NAME}`) || SITE_NAME}</title>
  </Helmet>
);

TitleComponent.propTypes = {
  title: PropTypes.string
};

export default TitleComponent;
