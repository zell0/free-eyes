import React from 'react';

import {
  StyledLogo, Link, Text, Image
} from './styles';
import img from 'assets/images/logo.png';

const Logo = () => (
  <StyledLogo>
    <Link exact to="/">
      <Image src={img} alt="Open your eyes" />
    </Link>
    <Text>Amazing World is opening in front of you</Text>
  </StyledLogo>
);

export default Logo;
