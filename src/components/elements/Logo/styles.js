import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

import { device } from 'constants/device';

export const StyledLogo = styled.div``;

export const Link = styled(NavLink)`
  display: block;
`;

export const Text = styled.div`
  font-size: 22px;
  font-weight: 700;
  font-variant: small-caps;
  color: ${({ theme }) => theme.GREY};
  margin-top: -5px;
  
  ${device.mobileL} {
    font-size: 16px;
  }
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  max-width: 337px;
`;
