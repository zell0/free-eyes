import React from 'react';

import SocialListItem from 'components/SocialListItem';
import { StyledSocial } from './styles';
import { socialList } from 'utils/data';

const SocialList = () => (
  <StyledSocial>
    {
      socialList.map((item, idx) => (
        <SocialListItem
          key={idx}
          title={item.title}
          href={item.url}
          srcImage={item.icon}
        />
      ))
    }
  </StyledSocial>
);

export default SocialList;
