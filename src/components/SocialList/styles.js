import styled from 'styled-components/macro';

export const StyledSocial = styled.div`
  display: inline-flex;
  justify-content: space-between;
  width: 80px;
`;
