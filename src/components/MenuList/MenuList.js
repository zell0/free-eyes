import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { StyledMenuList, Link, Item } from './styles';
import { menuList } from 'utils/data';

const MenuList = forwardRef(({ show, open }, ref) => (
  <StyledMenuList ref={ref} show={show} open={open}>
    {
      menuList.map((item, idx) => (
        <Item key={idx}>
          <Link
            exact
            to={item.url}
            activeClassName="active"
            show={show ? 1 : 0}
          >
            {item.txt}
          </Link>
        </Item>
      ))
    }
  </StyledMenuList>
));

MenuList.propTypes = {
  show: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired
};

MenuList.displayName = 'MenuList';

export default MenuList;
