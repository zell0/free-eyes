import styled, { css } from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

export const StyledMenuList = styled.ul`
  display: inline-flex;
  flex-flow: row nowrap;
  
  ${({ theme, show, open }) => !show && css`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: auto;
    max-height: 100%;
    flex-direction: column;
    background-color: ${theme.WHITE};
    padding-top: 50px;
    padding-bottom: 5px;
    box-shadow: 0 0 25px ${theme.GREY};
    transform: ${(!show && !open) ? 'translateX(-100%)' : 'translateX(0)'};
    transition: 0.2s ease;
    overflow-y: auto;
    z-index: 9;
  `}
`;

export const Item = styled.li``;

export const Link = styled(NavLink)`
  display: block;
  height: 100%;
  padding: 8px ${({ theme, show }) => (show ? '20px' : `${theme.INDENT}px`)};
  
  :hover,
  &.active {
    background-color: ${({ theme }) => theme.GREYLIGHT};
  }
`;
