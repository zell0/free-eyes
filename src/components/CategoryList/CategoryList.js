import React from 'react';
import PropTypes from 'prop-types';
import CategoryItem from 'components/CategoryItem';

import { StyledCategoryList } from './styles';

const CategoryList = ({ categories }) => (
  <StyledCategoryList>
    {
      categories.map((category) => (
        <CategoryItem
          key={category.id}
          href={category.url}
          srcImage={category.img_url}
          title={category.title}
        />
      ))
    }
  </StyledCategoryList>
);

CategoryList.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.object)
};

export default CategoryList;
