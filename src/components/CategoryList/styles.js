import styled from 'styled-components/macro';

export const StyledCategoryList = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  margin-bottom: 20px;
`;
