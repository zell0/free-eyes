import React from 'react';

import { ApiConsumer } from 'services/apiContext';

const withApi = (mapMethodsToProps) => (Wrapped) => (
  function withApi(props) {
    return (
      <ApiConsumer>
        {
          (api) => {
            const apiProps = mapMethodsToProps(api);

            return <Wrapped {...props} {...apiProps} />;
          }
        }
      </ApiConsumer>
    );
  }
);

export default withApi;
