import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Loader from 'components/elements/Loader';
import ErrorIndicator from 'components/ErrorIndicator';
import ErrorPage from 'pages/ErrorPage';

const withData = (View) => {
  class withData extends Component {
    getData = () => {
      const { match, actionRequest } = this.props;

      actionRequest(match.params.url);
    };

    componentDidMount() {
      this.getData();
    }

    componentDidUpdate(prevProps) {
      if (prevProps.match.url !== this.props.match.url
        || prevProps.location.search !== this.props.location.search) {
        this.getData();
      }
    }

    render() {
      const {
        data, dataById, loading, error
      } = this.props;

      if (loading) {
        return <Loader />;
      }

      if (error && error.type === 404) {
        return <ErrorPage />;
      }

      if (error) {
        return <ErrorIndicator />;
      }

      return <View {...this.props} data={data} dataById={dataById} />;
    }
  }

  withData.propTypes = {
    match: PropTypes.objectOf(PropTypes.any).isRequired,
    location: PropTypes.objectOf(PropTypes.any),
    data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    dataById: PropTypes.arrayOf(PropTypes.object),
    actionRequest: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool])
  };

  return withData;
};

export default withData;
