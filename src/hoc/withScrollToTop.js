import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import scrollIntoView from 'scroll-into-view';

const withScrollToTop = (View) => {
  const Comp = (props) => {
    const top = useRef();
    const history = useHistory();
    const { mobile, navigationHeight } = props;

    useEffect(() => {
      if (top.current.getBoundingClientRect().top < 0) {
        const offset = mobile ? navigationHeight + 15 : 15;

        scrollIntoView(top.current, {
          time: 400,
          align: {
            topOffset: offset
          }
        });
      }
    }, [history.location]); // eslint-disable-line

    return (
      <div ref={top}>
        <View {...props} />
      </div>
    );
  };

  Comp.propTypes = {
    mobile: PropTypes.bool,
    navigationHeight: PropTypes.number
  };

  Comp.displayName = 'Comp';
  return Comp;
};

export default compose(
  connect(
    ({ layout: { mobile, navigationHeight } }) => ({
      mobile,
      navigationHeight
    }),
    null
  ),
  withScrollToTop
);
