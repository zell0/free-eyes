import { combineReducers } from 'redux';

import articles from './articlesReducer';
import categories from './categoriesReducer';
import layout from './layoutReducer';

export default combineReducers({
  articles,
  categories,
  layout
});
