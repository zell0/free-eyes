import * as TYPES from 'store/types';

const initialState = {
  mobile: true,
  navigationHeight: null
};

const layoutReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case TYPES.SET_WEBSITE_LAYOUT:
      const { mobile, navigationHeight } = payload;

      return {
        mobile,
        navigationHeight
      };

    default:
      return state;
  }
};

export default layoutReducer;
