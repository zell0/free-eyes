import * as TYPES from 'store/types';

const initialState = {
  categories: {},
  categorySingle: {},
  loading: true,
  loadingCategory: true,
  error: null,
  errorSingle: null
};

const categoriesReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case TYPES.FETCH_CATEGORIES_REQUEST:
      return {
        ...state,
        categories: {},
        loading: true,
        error: null
      };

    case TYPES.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: payload,
        loading: false,
        error: null
      };

    case TYPES.FETCH_CATEGORIES_FAILURE:
      return {
        ...state,
        categories: {},
        loading: false,
        error: payload
      };

    case TYPES.FETCH_CATEGORY_SINGLE_REQUEST:
      return {
        ...state,
        categorySingle: {},
        loadingCategory: true,
        errorSingle: null
      };

    case TYPES.FETCH_CATEGORY_SINGLE_SUCCESS:
      return {
        ...state,
        categorySingle: payload,
        loadingCategory: false,
        errorSingle: null,
      };

    case TYPES.FETCH_CATEGORY_SINGLE_FAILURE:
      return {
        ...state,
        loadingCategory: false,
        errorSingle: payload
      };

    default:
      return state;
  }
};

export default categoriesReducer;
