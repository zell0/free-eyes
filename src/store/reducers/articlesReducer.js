import * as TYPES from 'store/types';

const initialState = {
  articleList: [],
  articleSingle: {},
  articlesPerPage: 4,
  currentPage: 1,
  loading: true,
  error: null
};

const articlesReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case TYPES.FETCH_ARTICLES_REQUEST:
      return {
        ...state,
        articleList: [],
        articleSingle: {},
        loading: true,
        error: null
      };

    case TYPES.FETCH_ARTICLES_SUCCESS:
      return {
        ...state,
        articleList: payload,
        loading: false,
        error: null
      };

    case TYPES.FETCH_ARTICLES_FAILURE:
      return {
        ...state,
        articleList: [],
        loading: false,
        error: payload
      };

    case TYPES.FETCH_ARTICLE_SINGLE_REQUEST:
      return {
        ...state,
        articleSingle: {},
        loading: true,
        error: null,
      };

    case TYPES.FETCH_ARTICLE_SINGLE_SUCCESS:
      return {
        ...state,
        articleSingle: payload,
        loading: false,
        error: null,
      };

    case TYPES.FETCH_ARTICLE_SINGLE_FAILURE:
      return {
        ...state,
        articleSingle: {},
        loading: false,
        error: payload,
      };

    case TYPES.SET_ARTICLE_RATE_SUCCESS:
      const { articleSingle } = state;

      return {
        ...state,
        articleSingle: {
          ...articleSingle,
          rating: {
            total: articleSingle.rating.total + payload,
            amount: articleSingle.rating.amount + 1
          }
        }
      };

    case TYPES.SET_CURRENT_PAGE:
      return {
        ...state,
        currentPage: payload
      };

    default:
      return state;
  }
};

export default articlesReducer;
