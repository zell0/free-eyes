import * as TYPES from 'store/types';

export const categoriesRequest = () => ({
  type: TYPES.FETCH_CATEGORIES_REQUEST
});

export const categoriesSuccess = (categories) => ({
  type: TYPES.FETCH_CATEGORIES_SUCCESS,
  payload: categories
});

export const categoriesFailure = (error) => ({
  type: TYPES.FETCH_CATEGORIES_FAILURE,
  payload: error
});

export const categorySingleRequest = (url) => ({
  type: TYPES.FETCH_CATEGORY_SINGLE_REQUEST,
  payload: url
});

export const categorySingleSuccess = (category) => ({
  type: TYPES.FETCH_CATEGORY_SINGLE_SUCCESS,
  payload: category
});

export const categorySingleFailure = (error) => ({
  type: TYPES.FETCH_CATEGORY_SINGLE_FAILURE,
  payload: error
});
