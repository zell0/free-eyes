import { SET_WEBSITE_LAYOUT } from 'store/types';

export const setWebsiteLayout = (payload) => ({
  type: SET_WEBSITE_LAYOUT,
  payload
});
