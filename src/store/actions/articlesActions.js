import * as TYPES from 'store/types';

export const articlesRequest = () => ({
  type: TYPES.FETCH_ARTICLES_REQUEST
});

export const articlesSuccess = (articles) => ({
  type: TYPES.FETCH_ARTICLES_SUCCESS,
  payload: articles
});

export const articlesFailure = (error) => ({
  type: TYPES.FETCH_ARTICLES_FAILURE,
  payload: error
});

export const articleSingleRequest = (url) => ({
  type: TYPES.FETCH_ARTICLE_SINGLE_REQUEST,
  payload: url
});

export const articleSingleSuccess = (article) => ({
  type: TYPES.FETCH_ARTICLE_SINGLE_SUCCESS,
  payload: article
});

export const articleSingleFailure = (error) => ({
  type: TYPES.FETCH_ARTICLE_SINGLE_FAILURE,
  payload: error
});

export const articleRateRequest = (id, value) => ({
  type: TYPES.SET_ARTICLE_RATE_REQUEST,
  payload: { id, value }
});

export const articleRateSuccess = (value) => ({
  type: TYPES.SET_ARTICLE_RATE_SUCCESS,
  payload: value
});

export const setCurrentPage = (value) => ({
  type: TYPES.SET_CURRENT_PAGE,
  payload: value
});
