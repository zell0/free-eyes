import { all } from 'redux-saga/effects';

import articlesSaga from './articlesSaga';
import categoriesSaga from './categoriesSaga';

export default function* () {
  yield all([
    articlesSaga(),
    categoriesSaga()
  ]);
}
