import {
  call, put, takeLatest
} from 'redux-saga/effects';

import {
  FETCH_ARTICLES_REQUEST,
  FETCH_ARTICLE_SINGLE_REQUEST,
  SET_ARTICLE_RATE_REQUEST
} from 'store/types';
import {
  articlesSuccess,
  articlesFailure,
  articleSingleSuccess,
  articleSingleFailure,
  articleRateSuccess
} from 'store/actions';

import api from 'services/api';
import { isUndefined } from 'utils';

function* fetchArticlesRequest() {
  try {
    const articles = yield call(api.getAllArticles);
    const articlesReversed = yield call([articles, 'reverse']);
    yield put(articlesSuccess(articlesReversed));
  } catch (e) {
    yield put(articlesFailure(e));
  }
}

function* fetchArticleSingleRequest({ payload }) {
  try {
    const article = yield call(api.getArticle, payload);
    if (isUndefined(article)) {
      yield put(articleSingleFailure({ type: 404 }));
      return;
    }
    yield put(articleSingleSuccess(article));
  } catch (e) {
    yield put(articleSingleFailure(e));
  }
}

function* setArticleRateRequest({ payload: { id, value } }) {
  try {
    yield call([localStorage, 'setItem'], `rate-${id}`, value);
    yield put(articleRateSuccess(value));
  } catch (e) {
    yield put(articleSingleFailure(e));
  }
}

export default function* () {
  yield takeLatest(FETCH_ARTICLES_REQUEST, fetchArticlesRequest);
  yield takeLatest(FETCH_ARTICLE_SINGLE_REQUEST, fetchArticleSingleRequest);
  yield takeLatest(SET_ARTICLE_RATE_REQUEST, setArticleRateRequest);
}
