import {
  call, put, takeLatest
} from 'redux-saga/effects';

import {
  FETCH_CATEGORIES_REQUEST,
  FETCH_CATEGORY_SINGLE_REQUEST
} from 'store/types';
import {
  articlesSuccess,
  categoriesSuccess,
  categoriesFailure,
  categorySingleSuccess,
  categorySingleFailure
} from 'store/actions';

import api from 'services/api';
import { isUndefined } from 'utils';

function* fetchCategoriesRequest() {
  try {
    const categories = yield call(api.getAllCategories);
    yield put(categoriesSuccess(categories));
  } catch (e) {
    yield put(categoriesFailure(e));
  }
}

function* fetchCategorySingleRequest({ payload }) {
  try {
    const category = yield call(api.getCategory, payload);
    if (isUndefined(category)) {
      yield put(categorySingleFailure({ type: 404 }));
      return;
    }
    const articlesById = yield call(api.getArticlesById, category.id);
    const articlesByIdReversed = yield call([articlesById, 'reverse']);
    yield put(articlesSuccess(articlesByIdReversed));
    yield put(categorySingleSuccess(category));
  } catch (e) {
    yield put(categorySingleFailure(e));
  }
}

export default function* () {
  yield takeLatest(FETCH_CATEGORIES_REQUEST, fetchCategoriesRequest);
  yield takeLatest(FETCH_CATEGORY_SINGLE_REQUEST, fetchCategorySingleRequest);
}
