import { createGlobalStyle } from 'styled-components';
import styledNormalize from 'styled-normalize';
import { darken } from 'polished';

export const GlobalStyle = createGlobalStyle`
  ${styledNormalize}

  * {
    box-sizing: border-box;

    &::before,
    &::after {
      box-sizing: border-box;
    }
  }
  html, body {
    height: 100%;
  }
  body {
    font-family: ${({ theme }) => theme.FONT_MAIN};
    font-size: 16px;
    min-width: 320px;
    min-height: 1px;
    position: relative;
    line-height: 1.5;
    color: ${({ theme }) => theme.BLACK};
    overflow-wrap: break-word;
    word-wrap: break-word;
    overflow-x: hidden;
    
    // Internet Explorer and EdgeHTML based
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    
    // Firefox
    scrollbar-width: thin;
    scrollbar-color: ${({ theme }) => theme.GREY} ${({ theme }) => theme.GREYLIGHT};
    
    // Webkit
    &::-webkit-scrollbar {
      width: 7px;
      height: 7px;
    }
    
    &::-webkit-scrollbar-track {
      background-color: ${({ theme }) => theme.GREYLIGHT};
    }
    
    &::-webkit-scrollbar-thumb {
      background-color: ${({ theme }) => theme.GREY};
      border-radius: 3px;
      
      &:hover {
        background-color: ${({ theme }) => darken(0.1, theme.GREY)};
      }
      
      &:active {
        background-color: ${({ theme }) => darken(0.25, theme.GREY)};
      }
    }

    &.overflow-hidden {
      overflow: hidden;
    }
  }
  a {
    color: ${({ theme }) => theme.BLACK};
    text-decoration: none;
    transition: .2s ease;

    &:active,
    &:focus {
      text-decoration: none;
    }
  }
  img {
    max-width: 100%;
    height: auto;
  }
  h1, h2, h3, h4, h5, h6 {
    color: ${({ theme }) => theme.BLACK};
  }
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
  #root {
    height: 100%;
  }
`;
