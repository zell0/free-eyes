const formControls = {
  name: {
    type: 'input',
    name: 'name',
    label: 'Your name',
    active: false,
    value: '',
    touched: false,
    valid: false,
    validationRules: {
      minLength: 2,
      isRequired: true
    }
  },
  email: {
    type: 'input',
    name: 'email',
    label: 'Your email',
    active: false,
    value: '',
    touched: false,
    valid: false,
    validationRules: {
      isEmail: true,
      isRequired: true
    }
  },
  phone: {
    type: 'input',
    name: 'phone',
    label: 'Your phone',
    active: false,
    value: '',
    touched: false,
    valid: false,
    validationRules: {
      isPhone: true,
      minLength: 10,
      isRequired: true
    }
  },
  message: {
    type: 'textarea',
    name: 'message',
    label: 'Your message',
    active: false,
    value: '',
    touched: false,
    valid: false,
    validationRules: {
      minLength: 5,
      isRequired: true
    }
  }
};

export default formControls;
