import vkIcon from 'assets/images/social/vk.svg';
import fbIcon from 'assets/images/social/fb.svg';
import twitterIcon from 'assets/images/social/twitter.svg';

const socialList = [
  {
    icon: vkIcon,
    title: 'VK',
    url: 'https://vk.com/club74792618'
  },
  {
    icon: fbIcon,
    title: 'Facebook',
    url: 'https://www.facebook.com/FreeEyesGroup'
  },
  {
    icon: twitterIcon,
    title: 'Twitter',
    url: 'https://twitter.com/free_eyes_com'
  },
];

export default socialList;
