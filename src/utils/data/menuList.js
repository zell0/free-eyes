const menuList = [
  {
    url: '/',
    txt: 'Home'
  },
  {
    url: '/about/',
    txt: 'About'
  },
  {
    url: '/contacts/',
    txt: 'Contacts'
  },
];

export default menuList;
