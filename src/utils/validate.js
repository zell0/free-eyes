const validate = (value, rules) => {
  let isValid = true;

  Object.keys(rules).forEach((rule) => {
    if (rules.hasOwnProperty(rule)) {
      switch (rule) {
        case 'isEmail': isValid = isValid && emailValidator(value); break;
        case 'isPhone': isValid = isValid && phoneValidator(value); break;
        case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]); break;
        case 'maxLength': isValid = isValid && maxLengthValidator(value, rules[rule]); break;
        case 'isRequired': isValid = isValid && requiredValidator(value); break;
        default: isValid = true;
      }
    }
  });

  return isValid;
};

const minLengthValidator = (value, minLength) => value.length >= minLength;

const maxLengthValidator = (value, maxLength) => value.length <= maxLength;

const requiredValidator = (value) => value.trim() !== '';

const emailValidator = (value) => {
  const re = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(value).toLowerCase());
};

const phoneValidator = (value) => {
  const re = /([0-9\s-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
  return re.test(String(value).toLowerCase());
};

export default validate;
