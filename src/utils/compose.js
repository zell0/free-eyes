const compose = (...funcs) => (component) => funcs.reduceRight((prev, f) => f(prev), component);

export default compose;
