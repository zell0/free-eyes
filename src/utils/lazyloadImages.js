const lazyloadImages = (root) => {
  let images;

  if ('IntersectionObserver' in window) {
    images = root.querySelectorAll('.lazy');
    const imageObserver = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          const image = entry.target;
          image.src = image.dataset.src;
          image.classList.remove('lazy');
          imageObserver.unobserve(image);
        }
      });
    }, {
      rootMargin: '0px 0px 200px 0px'
    });

    images.forEach((image) => {
      imageObserver.observe(image);
    });
  } else {
    let lazyloadThrottleTimeout;
    images = document.querySelectorAll('.lazy');

    const lazyload = () => {
      if (lazyloadThrottleTimeout) {
        clearTimeout(lazyloadThrottleTimeout);
      }

      lazyloadThrottleTimeout = setTimeout(() => {
        const scrollTop = window.pageYOffset;
        images.forEach((image) => {
          const img = image;
          if (img.offsetTop < (window.innerHeight + scrollTop)) {
            img.src = img.dataset.src;
            img.classList.remove('lazy');
          }
        });
        if (images.length === 0) {
          document.removeEventListener('scroll', lazyload);
          window.removeEventListener('resize', lazyload);
          window.removeEventListener('orientationChange', lazyload);
        }
      }, 20);
    };

    document.addEventListener('scroll', lazyload);
    window.addEventListener('resize', lazyload);
    window.addEventListener('orientationChange', lazyload);
  }
};

export default lazyloadImages;
