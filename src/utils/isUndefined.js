const isUndefined = (data) => typeof data === 'undefined';

export default isUndefined;
