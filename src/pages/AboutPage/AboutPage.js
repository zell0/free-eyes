import React from 'react';

import TitleComponent from 'components/elements/TitleComponent';
import Typography from 'components/elements/Typography';
import { withScrollToTop } from 'hoc';

const AboutPage = () => (
  <>
    <TitleComponent title="About" />
    <Typography>About</Typography>
    <p>Some become travelers, others devote their lives to tourism and visit the farthest corners of our vast planet.</p>
  </>
);

export default withScrollToTop(AboutPage);
