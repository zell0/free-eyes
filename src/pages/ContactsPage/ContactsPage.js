import React from 'react';

import TitleComponent from 'components/elements/TitleComponent';
import Typography from 'components/elements/Typography';
import Form from 'components/forms/Form';
import { withScrollToTop } from 'hoc';

const ContactsPage = () => (
  <>
    <TitleComponent title="Contacts" />
    <Typography>Contacts</Typography>
    <Form />
  </>
);

export default withScrollToTop(ContactsPage);
