import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';

import Article from 'components/Article';
import { withData, withScrollToTop } from 'hoc';
import { articleSingleRequest, articleRateRequest } from 'store/actions';
import TitleComponent from 'components/elements/TitleComponent';

const ArticlePageContainer = ({ data, articleRateRequest }) => (
  <>
    <TitleComponent title={data.title} />
    <Article article={data} setRateArticle={articleRateRequest} />
  </>
);

ArticlePageContainer.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  articleRateRequest: PropTypes.func.isRequired
};

export default compose(
  connect(
    ({ articles: { articleSingle, loading, error } }) => ({
      data: articleSingle,
      loading,
      error
    }),
    {
      actionRequest: articleSingleRequest,
      articleRateRequest
    }
  ),
  withScrollToTop,
  withData
)(ArticlePageContainer);
