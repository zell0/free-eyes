import React from 'react';

import TitleComponent from 'components/elements/TitleComponent';
import Typography from 'components/elements/Typography';

const ErrorPage = () => (
  <>
    <TitleComponent title="Page not found" />
    <Typography>Page not found</Typography>
  </>
);

export default ErrorPage;
