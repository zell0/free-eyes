import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import ArticleListContainer from 'components/ArticleListContainer';
import Typography from 'components/elements/Typography';
import TitleComponent from 'components/elements/TitleComponent';
import { withData, withScrollToTop } from 'hoc';
import { articlesRequest } from 'store/actions';

const HomePageContainer = ({ data, location }) => {
  if (location.search.includes('search')) {
    const parsed = parse(location.search).search.toLowerCase();
    const searchResults = data.filter(
      (article) => article.title.toLowerCase().includes(parsed) || article.content.toLowerCase().includes(parsed)
    );

    return (
      <>
        <TitleComponent title={`Search results for: ${parsed}`} />
        <Typography>Search results:</Typography>
        <ArticleListContainer
          articles={searchResults}
          location={location}
          noDataMessage="No results were found for your request"
        />
      </>
    );
  }

  return (
    <>
      <TitleComponent />
      <ArticleListContainer
        articles={data}
        location={location}
        noDataMessage="No articles yet"
      />
    </>
  );
};

HomePageContainer.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any).isRequired,
  location: PropTypes.objectOf(PropTypes.any)
};

export default compose(
  connect(
    ({ articles: { articleList, loading, error } }) => ({
      data: articleList,
      loading,
      error
    }),
    {
      actionRequest: articlesRequest
    }
  ),
  withScrollToTop,
  withData
)(HomePageContainer);
