import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';

import Category from 'components/Category';
import ArticleListContainer from 'components/ArticleListContainer';
import TitleComponent from 'components/elements/TitleComponent';
import { withData, withScrollToTop } from 'hoc';
import { categorySingleRequest } from 'store/actions';

const CategoryPageContainer = ({ data, dataById, location }) => (
  <>
    <TitleComponent title={data.title} />
    <Category category={data} />
    <ArticleListContainer
      articles={dataById}
      location={location}
      noDataMessage="No articles in this category"
    />
  </>
);

CategoryPageContainer.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  dataById: PropTypes.arrayOf(PropTypes.any).isRequired,
  location: PropTypes.objectOf(PropTypes.any)
};

export default compose(
  connect(
    ({
      articles: { articleList },
      categories: { categorySingle, loadingCategory, errorSingle }
    }) => ({
      data: categorySingle,
      dataById: articleList,
      loading: loadingCategory,
      error: errorSingle
    }),
    {
      actionRequest: categorySingleRequest
    }
  ),
  withScrollToTop,
  withData
)(CategoryPageContainer);
