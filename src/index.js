import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';

import App from 'components/App';
import ErrorBoundary from 'components/ErrorBoundary';
import { theme } from 'constants/theme';

import store from 'store';

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundary>
      <Router>
        <ThemeProvider theme={theme}>
          <App />
        </ThemeProvider>
      </Router>
    </ErrorBoundary>
  </Provider>,
  document.getElementById('root')
);
