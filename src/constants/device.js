import { mediaQuery } from 'utils';

const size = {
  mobileL: 425,
  tablet: 767,
  laptop: 1024
};

export const device = {
  mobileL: mediaQuery(size.mobileL),
  tablet: mediaQuery(size.tablet),
  laptop: mediaQuery(size.laptop)
};
