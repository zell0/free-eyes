export const theme = {
  CONTAINER_WIDTH: 1200,
  GUTTER: 30,
  INDENT: 15,

  FONT_MAIN: `'PT Sans', sans-serif`,

  BLACK: '#000000',
  WHITE: '#FFFFFF',
  PRIMARY: '#416099',
  SECONDARY: '#2A6496',
  ACTIVE: '#66afe9',
  ERROR: '#F6182A',
  GREY: '#7e979d',
  GREYLIGHT: '#d5dddf',
  GREYLIGHTEST: '#f8f9f9',
  YELLOW: '#EFD44E'
};
